//
//  Car.swift
//  TestLib
//
//  Created by poisson florent on 23/07/2018.
//  Copyright © 2018 poisson florent. All rights reserved.
//

import Foundation

public class Car {
    
    var name: String
    var miles: Int
    
    public init(name: String, miles: Int) {
        self.name = name
        self.miles = miles
    }
    
    public func addMiles(miles: Int) {
        self.miles += miles
    }
    
    public var description: String {
        return "Car '\(name)' has \(miles) miles."
    }
    
}
